<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/functions.php';


/**
 * Returns the theme's images folder path.
 *
 * @return bab_Path
 */
function theme_crm_like_getImagePath($directory)
{
    $addon = bab_getAddonInfosInstance('theme_crm_like');

    $ovidentiapath = realpath('.');

    $uploadPath = new bab_Path($ovidentiapath, 'images',  $addon->getRelativePath(), $directory);
    if (!$uploadPath->isDir()) {
        $uploadPath->createDir();
    }

    return $uploadPath;
}



/**
 * Displays a form to edit the theme configuration.
 */
function theme_crm_like_editConfiguration()
{
    bab_Functionality::includefile('Icons');
    $W = bab_Widgets();
    $page = $W->BabPage();

    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/theme_crm_like/global');

    $form = $W->Form();

    $form->addClass(Func_Icons::ICON_LEFT_16);

    $siteSitemap = bab_Sitemap::getSiteSitemap();

    $nodeSection = $W->Section(
        theme_crm_like_translate('Navigation nodes'),
        $W->VBoxItems(
            theme_crm_like_LabelledWidget(
                theme_crm_like_translate('Top navigation node'),
                $W->SitemapItemPicker()
                    ->setSitemap($siteSitemap->getSitemapName())
                    ->basenode('DGAll')
                    ->setName('topNavigationNode')
                    ->setValue($registry->getValue('topNavigationNode'))
            ),
            theme_crm_like_LabelledWidget(
                theme_crm_like_translate('Bottom navigation node'),
                $W->SitemapItemPicker()
                    ->setSitemap($siteSitemap->getSitemapName())
                    ->basenode('DGAll')
                    ->setName('bottomNavigationNode')
                    ->setValue($registry->getValue('bottomNavigationNode'))
            )
        )
    );
    $faviconImagePicker = $W->ImagePicker();
    $faviconImagePicker->setSizePolicy('widget-100pc')
        ->oneFileMode()
        ->setDimensions(32, 32)
        ->setTitle(theme_crm_like_translate('favicon'))
        ->setName('faviconImage');

    $bannerImagePicker = $W->ImagePicker();
    $bannerImagePicker->setSizePolicy('widget-100pc')
        ->oneFileMode()
        ->setDimensions(200, 200)
        ->setTitle(theme_crm_like_translate('Banner image'))
        ->setName('bannerImage');

    $logoImagePicker = $W->ImagePicker();
    $logoImagePicker->setSizePolicy('widget-100pc')
        ->oneFileMode()
        ->setDimensions(200, 200)
        ->setTitle(theme_crm_like_translate('Logo image'))
        ->setName('logoImage');

    $imagesSection = $W->Section(
        theme_crm_like_translate('Images'),
        $W->FlowItems(
            $faviconImagePicker,
            $bannerImagePicker,
            $logoImagePicker
        )->setHorizontalSpacing(2, 'em')
    );

    $faviconImageFolder = theme_crm_like_getImagePath('favicon');
    $faviconImagePicker->importPath($faviconImageFolder, 'UTF-8');

    $logoImageFolder = theme_crm_like_getImagePath('logo');
    $logoImagePicker->importPath($logoImageFolder, 'UTF-8');

    $bannerImageFolder = theme_crm_like_getImagePath('banner');
    $bannerImagePicker->importPath($bannerImageFolder, 'UTF-8');

    $colorSection = $W->Section(
        theme_crm_like_translate('Colors'),
        $W->VBoxItems(
            theme_crm_like_LabelledWidget(
                theme_crm_like_translate('Header background'),
                $W->ColorPicker()
                    ->setName('headerBackgroundColor')
                    ->setValue(substr($registry->getValue('headerBackgroundColor'), 1))
            ),
            theme_crm_like_LabelledWidget(
                theme_crm_like_translate('Main color'),
                $W->ColorPicker()
                    ->setName('mainColor')
                    ->setValue(substr($registry->getValue('mainColor'), 1))
            )
        )
    );

    $advancedSection = $W->Section(
        theme_crm_like_translate('Advanced'),
        $W->VBoxItems(
            theme_crm_like_LabelledWidget(
                theme_crm_like_translate('Global css'),
                $W->TextEdit()
                    ->setLines(10)
                    ->addClass('widget-100pc')
                    ->setSizePolicy('widget-100pc')
                    ->setName('globalCss')
                    ->setValue($registry->getValue('globalCss'))
            )
        )
    );

    $otherSection = $W->Section(
        theme_crm_like_translate('Other'),
        $W->VBoxItems(
            theme_crm_like_LabelledWidget(
                theme_crm_like_translate('Max width'),
                $W->LineEdit()
                    ->setName('maxWidth')
                    ->setValue($registry->getValue('maxWidth'))
            ),
            theme_crm_like_LabelledWidget(
                theme_crm_like_translate('Header height'),
                $W->LineEdit()
                    ->setName('headerHeight')
                    ->setValue($registry->getValue('headerHeight'))
            ),
            theme_crm_like_LabelledWidget(
                theme_crm_like_translate('Header text'),
                $W->LineEdit()
                    ->setName('headerText')
                    ->setValue($registry->getValue('headerText'))
            ),
            theme_crm_like_LabelledWidget(
                theme_crm_like_translate('Sub-header text'),
                $W->LineEdit()
                    ->setName('subHeaderText')
                    ->setValue($registry->getValue('subHeaderText'))
            )
        )
    );

    $form->addItem(
        $W->VBoxItems(
            $W->Frame(null,
                $W->VBoxItems(
                    $W->FlowItems(
                        $W->VBoxItems(
                            $colorSection,
                            $nodeSection,
                            $otherSection
                        )->setVerticalSpacing(2, 'em')
                        ->setSizePolicy('widget-67pc'),
                        $W->VBoxItems(
                            $imagesSection
                        )->setVerticalSpacing(2, 'em')
                        ->setSizePolicy('widget-33pc')
                    )->setVerticalAlign('top'),
                    $advancedSection->setFoldable(true)
                )->setVerticalSpacing(2, 'em')
            )->setName('configuration'),

            $W->FlowItems(
                $W->SubmitButton()
                    ->setName('idx[save]')
                    ->setLabel(theme_crm_like_translate('Save configuration')),
                $W->SubmitButton()
                    ->setName('idx[cancel]')
                    ->setLabel(theme_crm_like_translate('Cancel'))
            )->setHorizontalSpacing(1, 'em')

        )->setVerticalSpacing(3, 'em')
    );

    $form->addClass('widget-bordered');

    $form->setHiddenValue('tg', bab_rp('tg'));

    $page->setTitle(theme_crm_like_translate('Theme configuration'));
    $page->addItem($form);

    $page->displayHtml();
}




function theme_crm_like_getImageUrl($imageName, $baseFolder = '')
{
    $W = bab_Widgets();

    $imagesPath = theme_crm_like_getImagePath($imageName);
    try {
        $imagesPath->deleteDir();
    } catch (bab_FolderAccessRightsException $e) { }
    $imagesPath->createDir();

    $imagePicker = $W->ImagePicker()
        ->setName($imageName . 'Image');
    if ($files = $imagePicker->getTemporaryFiles()) {
        foreach ($files as $file) {
            rename($file->getFilePath()->toString(), $imagesPath->toString() . '/' . $file->getFileName());
        }
    }

    $imageUrl = "''";
    foreach ($imagesPath as $imagePath) {
        $imageFilename = basename($imagePath->toString());
        $imageFilename = str_replace("'", "%27", $imageFilename);
        $imageUrl = "'" . $baseFolder . $imageName . '/' .  $imageFilename . "'";
        break;
    }

    return $imageUrl;
}

/**
 * Saves the posted configuration.
 *
 * @param array $configuration
 */
function theme_crm_like_saveConfiguration($configuration)
{
    $registry = bab_getRegistryInstance();
    $registry->changeDirectory('/theme_crm_like/global');

    if (isset($configuration['topNavigationNode']) && is_string($configuration['topNavigationNode'])) {
        $registry->setKeyValue('topNavigationNode', $configuration['topNavigationNode']);
    }
    if (isset($configuration['bottomNavigationNode']) && is_string($configuration['bottomNavigationNode'])) {
        $registry->setKeyValue('bottomNavigationNode', $configuration['bottomNavigationNode']);
    }
    if (isset($configuration['maxWidth']) && is_string($configuration['maxWidth'])) {
        if (!preg_match('/[0-9\.](px|em|\%|pt|cm|mm|rem)/', $configuration['maxWidth'])) {
            $configuration['maxWidth'] = 0;
        }
        $registry->setKeyValue('maxWidth', $configuration['maxWidth']);
    }
    if (isset($configuration['headerHeight']) && is_string($configuration['headerHeight'])) {
        if (!preg_match('/[0-9\.](px|em|\%|pt|cm|mm|rem)/', $configuration['headerHeight'])) {
            $configuration['headerHeight'] = 0;
        }
        $registry->setKeyValue('headerHeight', $configuration['headerHeight']);
    }
    if (isset($configuration['headerText']) && is_string($configuration['headerText'])) {
        $registry->setKeyValue('headerText', $configuration['headerText']);
    }
    if (isset($configuration['subHeaderText']) && is_string($configuration['subHeaderText'])) {
        $registry->setKeyValue('subHeaderText', $configuration['subHeaderText']);
    }
    if (isset($configuration['globalCss']) && is_string($configuration['globalCss'])) {
        $value = $configuration['globalCss'];
        $registry->setKeyValue('globalCss', $value);
    }
    $registry->setKeyValue('headerBackgroundColor', '#' . $configuration['headerBackgroundColor']);
    $registry->setKeyValue('mainColor', '#' . $configuration['mainColor']);


    $addon = bab_getAddonInfosInstance('theme_crm_like');

    $registry->setKeyValue('faviconImage', theme_crm_like_getImageUrl('favicon', 'images/' . $addon->getRelativePath()));

    $registry->setKeyValue('bannerImage', theme_crm_like_getImageUrl('banner'));

    $registry->setKeyValue('logoImage', theme_crm_like_getImageUrl('logo'));


    /* @var $Less Func_Less */
    $Less = bab_functionality::get('less');

    $compiledCssPath = new bab_Path(theme_crm_like_getCompiledCssPath());
    foreach ($compiledCssPath as $file) {
        if (!$file->isDir()) {
            $file->delete();
        }
    }
    try {
        $Less->removeCompiledFiles();
    } catch(bab_FileAccessRightsException $e) {
        var_dump($e->getCode());
        var_dump($e);
        die;
        bab_debug($e->getMessage());
    }

}



// Exécution

if (!bab_isUserAdministrator()) {
    $babBody->addError(theme_crm_like_translate('Access denied.'));
    return;
}

$idx = bab_rp('idx', 'edit');


if (is_array($idx)) {
    list($idx,) = each($idx);
}


$msg = bab_rp('msg', null);
$errmsg = bab_rp('errmsg', null);
if (isset($errmsg)) {
    $babBody->addError($errmsg);
}
if (isset($msg)) {
    $babBody->addMessage($msg);
}

switch ($idx) {

    case 'save':
        $addon = bab_getAddonInfosInstance('theme_crm_like');
        $configuration = bab_rp('configuration', array());
        theme_crm_like_saveConfiguration($configuration);
        theme_crm_like_redirect($addon->getUrl().'configuration&idx=edit&msg='.urlencode(theme_crm_like_translate('Configuration saved')));
        break;

    case 'edit':
    default:
        theme_crm_like_editConfiguration();
        break;
}
