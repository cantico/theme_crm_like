<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';


/**
 * Called by ovidentia core when upgrading addon.
 *
 * @param string $version_base
 * @param string $version_ini
 * @return boolean
 */
function theme_crm_like_upgrade($version_base, $version_ini)
{
    require_once $GLOBALS['babInstallPath'] . 'utilit/upgradeincl.php';
    include_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';

    $addon = bab_getAddonInfosInstance('theme_crm_like');
    if (function_exists('bab_removeAddonEventListeners')) {
        bab_removeAddonEventListeners('theme_crm_like');
        $addon->addEventListener('bab_eventBeforePageCreated', 'theme_crm_like_addCssAndJs', 'init.php', -10);
        $addon->registerFunctionality('Ovml/Function/ThemeCrmLikeGetRegistryValue', 'ovml_configuration.php');
        $addon->registerFunctionality('Ovml/Function/ThemeCrmLikeImageBase64', 'ovml_configuration.php');
    } else {
        // We set a low priority on the BeforePageCreated event to ensure that the skin's css is added after default css.
        // It must also be executed after other addon may have changed the global babSkin variable.
        bab_addEventListener('bab_eventBeforePageCreated', 'theme_crm_like_addCssAndJs', 'addons/theme_crm_like/init.php', 'theme_crm_like', -10);

        $func = new bab_functionalities;
        $func->register('Ovml/Function/ThemeCrmLikeGetRegistryValue', dirname(__FILE__).'/ovml_configuration.php');
        $func->register('Ovml/Function/ThemeCrmLikeImageBase64', dirname(__FILE__).'/ovml_configuration.php');
    }

    return true;
}



/**
 * Called by ovidentia core when deleting addon.
 */
function theme_crm_like_onDeleteAddon()
{
    include_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';
    include_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';

    if (function_exists('bab_removeAddonEventListeners')) {
        $addon = bab_getAddonInfosInstance('theme_crm_like');
        bab_removeAddonEventListeners('theme_crm_like');
        $addon->unregisterFunctionality('Ovml/Function/ThemeCrmLikeGetRegistryValue');
        $addon->unregisterFunctionality('Ovml/Function/ThemeCrmLikeImageBase64');
    } else {
        bab_removeEventListener('bab_eventBeforePageCreated', 'theme_crm_like_addCssAndJs', 'addons/theme_crm_like/init.php');
        $func = new bab_functionalities;
        $func->unregister('Ovml/Function/ThemeCrmLikeGetRegistryValue');
        $func->unregister('Ovml/Function/ThemeCrmLikeImageBase64');
    }

    return true;
}




/**
 * Adds theme specific css and javascript to the page.
 *
 * Must be executed with a low priority on the BeforePageCreated event to ensure that the skin's css is added after default css.
 * It must also be executed after other addon may have changed the global babSkin variable.
 */
function theme_crm_like_addCssAndJs()
{
    if (bab_isAjaxRequest()) {
        return;
    }
    global $babSkin;
    if ('theme_crm_like' !== $babSkin) {
        return;
    }

    $babBody = bab_getBody();

     $Icons = bab_functionality::get('Icons');
     $Icons->includeCss();

    $jquery = bab_functionality::get('jquery');
    $jquery->includeCore();

    /* @var $Less Func_Less */
    if ($Less = @bab_functionality::get('less')) {
        $Less->setCompiledCssPath(theme_crm_like_getCompiledCssPath());
        $Less->setCompiledCssBaseUrl(theme_crm_like_getCompiledCssBaseUrl());

        $variables = theme_crm_like_getLessVariables();

        $rootPath = realpath('.');
        $addon = bab_getAddonInfosInstance('theme_crm_like');
        $stylesPath = $rootPath . '/' . $addon->getThemePath() . 'styles/';

        try {
            $stylesheet = $Less->getCssUrl($stylesPath.'style.less', $variables, false);
            $babBody->addStyleSheet($stylesheet);
        } catch (Exception $e) {
            bab_debug($e->getMessage());
        }
    }

}
