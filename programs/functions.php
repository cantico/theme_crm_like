<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';





/**
 * @param string $str
 *
 * @return string
 */
function theme_crm_like_translate($str)
{
    return bab_translate($str, 'theme_crm_like');
}




function theme_crm_like_redirect($url, $message = null)
{
	global $babBody;

	if (null === $url) {
		die('<script type="text/javascript">history.back();</script>');
	}

	header('Location: ' . $url);
	die;
}


/**
 *
 * @param string        $text
 * @param Widget_Widget $widget
 */
function theme_crm_like_LabelledWidget($text, $widget)
{
    $W = bab_Widgets();
    $label = $W->Label($text);
    $label->setSizePolicy('widget-15em');
    $label->setAssociatedWidget($widget);
    $label->colon();

    $labelledWidget =  $W->FlowItems($label, $widget);
    $labelledWidget->setHorizontalSpacing(1, 'em');

    return $labelledWidget;
}



/**
 *
 * @param string		$text
 * @param Widget_Widget	$widget
 * @param string		$name
 * @param bool			$checked
 * @return unknown
 */
function theme_crm_like_OptionalLabelledWidget($text, $widget, $name, $checked)
{
    $W = bab_Widgets();
    $label = $W->Label($text);
    $label->setSizePolicy('widget-10em');
    $label->setAssociatedWidget($widget);
    $label->colon();

    $checkbox = $W->CheckBox();
    $checkbox->setAssociatedDisplayable($widget);
    $checkbox->setName($name);
    $checkbox->setUncheckedValue('0');
    $checkbox->setCheckedValue($widget->getId());
    if ($checked) {
        $checkbox->setValue($widget->getId());
    }
    $checkbox->setSizePolicy('widget-5em');
    $labelledWidget = $W->FlowItems($label, $checkbox, $widget);
    $labelledWidget->setHorizontalSpacing(1, 'em');

    return $labelledWidget;
}





function theme_crm_like_shorten_values(&$defaultValues, $reg)
{
	$values = array();

	foreach ($defaultValues as $key => $value){
		$values[$key] = $reg->getValue($key, $value);
	}

	return $values;
}


/**
 * The folder path where the compiled css file will be stored.
 * @return string
 */
function theme_crm_like_getCompiledCssPath()
{
    $rootPath = realpath('.');
    $addon = bab_getAddonInfosInstance('theme_crm_like');
    $compiledCssPath = $rootPath . '/images/' . $addon->getRelativePath();

    return $compiledCssPath;
}


/**
 * The base url to access the compliled css files.
 * @return string
 */
function theme_crm_like_getCompiledCssBaseUrl()
{
    $addon = bab_getAddonInfosInstance('theme_crm_like');
    return  'images/' . $addon->getRelativePath();
}


/**
 *
 * @param string $configuration
 * @return array
 */
function theme_crm_like_getLessVariables($configuration = 'global')
{
	$defaultValues = array(
        'headerBackgroundColor' => '#26AADA',
		'mainColor'				=> '#DDDDDD',

        'bannerImage'           => "''",
        'logoImage'             => "''",
		'faviconImage'          => "''",

        'maxWidth'              => '0',
	    'headerHeight'          => '0'
	);

	$values = array();

	foreach ($defaultValues as $key => $value) {
	    $values[$key] = theme_crm_like_getConfigurationValue('/theme_crm_like/' . $configuration, $key, $value);
	}

	return $values;
}





/**
 *
 * @param string $registryDirectory
 * @param string $key
 * @param string $default
 * @return mixed
 */
function theme_crm_like_getConfigurationValue($registryDirectory, $key, $default = null)
{
    if (isset($GLOBALS[$registryDirectory . '/' . $key])) {
        // The global variable can override
        $value = $GLOBALS[$registryDirectory . '/' . $key];
    } else {

        // Selection du registre du theme
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory($registryDirectory);

        $value = $registry->getValue($key, $default);
    }

    return $value;
}

